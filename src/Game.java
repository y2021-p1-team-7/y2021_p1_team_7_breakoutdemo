import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class Game extends Application{

	public static void main(String[] args) {
		launch(args);
	}
	
	
	@Override
	public void start(Stage stage) throws Exception {
		// TODO Auto-generated method stub
		stage.setTitle("Breakout");
		stage.setResizable(false);
		BorderPane rootNode = new BorderPane();
		
		BallWorld bw = new BallWorld();
		bw.setPrefWidth(350);
		bw.setPrefHeight(500);
		
		
		rootNode.setCenter(bw);
		
		
		Scene scene = new Scene(rootNode);
		
		
		String path = getClass().getClassLoader().getResource("resources/ball.png").toString();
		Image img = new Image(path);
		
		String path2 = getClass().getClassLoader().getResource("resources/paddle.png").toString();
		Image img2 = new Image(path2);
		
		String path3 = getClass().getClassLoader().getResource("resources/brick.png").toString();
		Image img3 = new Image(path3);
		
		Ball b = new Ball(img, 3, 3);
		b.setFitWidth(25);
		b.setFitHeight(25);
		
		Paddle p = new Paddle(img2);
		p.setFitWidth(80);
		p.setY(500 - p.getHeight());
		p.setX(175 - p.getWidth()/2);
		
		Brick br = new Brick(img3);
		br.setFitWidth(80);
		br.setY(250);
		br.setX(280);
		
		bw.setOnMouseMoved(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				// Add your code here
				p.setX(event.getX());
				if(p.getX() <= 0) {
					p.setX(0);
				}
				if(p.getX() + p.getWidth() >= 350) {
					p.setX(350 - p.getWidth());
				}
			}});
		
		
		bw.add(b);
		bw.add(p);
		bw.add(br);
		
		bw.start();	
		
		stage.setScene(scene);
		stage.show();
		bw.requestFocus();
		
		bw.setOnKeyPressed(new EventHandler<KeyEvent>() {

			@Override
			public void handle(KeyEvent event) {
				// TODO Auto-generated method stub
				bw.addKey(event.getCode());
			}});
		
		bw.setOnKeyReleased(new EventHandler<KeyEvent>() {

			@Override
			public void handle(KeyEvent event) {
				// TODO Auto-generated method stub
				bw.removeKey(event.getCode());
			}
			
		});

	}
	
}
