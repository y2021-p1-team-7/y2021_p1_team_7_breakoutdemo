import javafx.scene.control.Label;
import javafx.scene.text.Font;

public class Score extends Label{

	private int score;
	
	public Score() {
		score = 0;
		updateDisplay();
		setFont(new Font(30));
	}
	
	public void updateDisplay() {
		setText(String.valueOf(score));
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
		updateDisplay();
	}
}
