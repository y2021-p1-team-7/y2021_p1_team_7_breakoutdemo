import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class Ball extends Actor{

	private double dx;
	private double dy;
	private boolean flag = false;
	
	public Ball(Image image, double dx, double dy) {
		this.setImage(image);
		this.dx = dx;
		this.dy = dy;
	}
	
	@Override
	public void act(long now) {
		// TODO Auto-generated method stub
		move(dx, dy);
		
		if(getX() + getWidth() >= getWorld().getWidth() || getX() <= 0) {
			dx = -dx;
		}
		
		if(getY() <= 0) {
			dy = -dy;
		}
		
		if(getY() + getHeight() >= getWorld().getHeight()) {
			dy = -dy;
			BallWorld b = (BallWorld)getWorld();
			b.getScore().setScore(b.getScore().getScore() - 1000);
		}
		Paddle gamePaddle = (Paddle)(getWorld().getChildren().get(0));
		double paddleX = gamePaddle.getX();
		
		if(!flag && getOneIntersectingObject(Paddle.class) != null) {
			if(getX() > paddleX - this.getWidth()/2 && getX() < paddleX - this.getWidth()/6) {
				//left bounce
				dx = -dx;
				dy = dy;
				gamePaddle.setDirection("left");
			}else if(getX() >= paddleX - this.getWidth()/6 && getX() < paddleX + this.getWidth()/6) {
				//middle bounce
				dy = dy;
			}else if(getX() >= paddleX + this.getWidth()/6 && getX() < paddleX + this.getWidth()/2) {
				//right bounce
				dx = -dx;
				dy = -dy;
				gamePaddle.setDirection("right");
			}else if(getX() >= paddleX + this.getWidth()/2) {
				//left corner bounce
				dx = -dx;
				dy = 0.25 * - dy;
			}else if(getX() >= paddleX - this.getWidth()/2) {
				//right corner bounce
				dx = dx;
				dy = 0.25 * dy;
			}
			flag = true;
		}
		
		Object inter = getOneIntersectingObject(Brick.class);
		if(inter != null) {
			if(getX() >= ((Brick) inter).getX() && getX() <= ((Brick) inter).getX() + ((Brick) inter).getWidth()) {
				dy = -dy;
			} else if (getY() >= ((Brick) inter).getY() && getY() <= ((Brick) inter).getY() + ((Brick) inter).getHeight()) {
				dx = -dx;
			} else {
				dy = -dy;
				dx = -dx;
			}
			((Brick) inter).getWorld().remove((Brick) inter);
			BallWorld b = (BallWorld)getWorld();
			b.getScore().setScore(b.getScore().getScore() + 100);
		}
	}

}
