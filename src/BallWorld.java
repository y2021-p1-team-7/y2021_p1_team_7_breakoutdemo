
public class BallWorld extends World{

	private Score score;
	
	public BallWorld() {
		super();
		score = new Score();
		getChildren().add(score);
	}
	
	public Score getScore() {
		return score;
	}
	
	@Override
	public void act(long now) {}

}
