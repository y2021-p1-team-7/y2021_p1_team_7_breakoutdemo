import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;

public class Paddle extends Actor{
	public boolean moving = false;
	private String direction = null;
	
	public Paddle(Image img) {
		this.setImage(img);
	}
	
	public boolean isMoving() {
		return moving;
	}

	public void setMoving(boolean m) {
		moving = m;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String d) {
		direction = d;
	}

	@Override
	public void act(long now) {
		// TODO Auto-generated method stub
		if(getWorld().isKeyDown(KeyCode.LEFT)) {
			move(-5,0);
			moving = true;
			direction = "left";
		}
		moving = false;
		direction = null;
		if(getWorld().isKeyDown(KeyCode.RIGHT)) {
			move(5,0);
			moving = true;
			direction = "right";
		}
		moving = false;
		direction = null;
		if(getX() <= 0) {
			setX(0);
		}
		if(getX() + getWidth() >= 350) {
			setX(350 - getWidth());
		}
	}

}
