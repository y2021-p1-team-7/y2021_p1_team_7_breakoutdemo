import java.util.ArrayList;
import java.util.HashSet;

import javafx.animation.AnimationTimer;
import javafx.scene.Node;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;

public abstract class World extends Pane{
	
	private AnimationTimer timer;
	
	private HashSet<KeyCode> keys = new HashSet<KeyCode>();
	
	public World() {
		timer = new AnimationTimer() {
			
			@Override
			public void handle(long now) {
				act(now);
				for(Actor a : getObjects(Actor.class)) {
					a.act(now);
				}
			}
			
		};
	}
	
	public abstract void act(long now);
	
	public void add(Actor actor) {
		getChildren().add(actor);
	}

	public void remove(Actor actor) {
		getChildren().remove(actor);
	}
	
	public void start() {
		timer.start();
	}
	
	public void stop() {
		timer.stop();
	}
	
	public void addKey(KeyCode key) {
		keys.add(key);
	}
	
	public void removeKey(KeyCode key) {
		keys.remove(key);
	}
	
	public Boolean isKeyDown(KeyCode key) {
		if(keys.contains(key)) {
			return true;
		} else {
			return false;
		}
	}
	
	public <A extends Actor> java.util.List<A> getObjects(java.lang.Class<A> cls){
		ArrayList<A> temp = new ArrayList<A>();
		for(Node a : getChildren()) {
			if(cls.isInstance(a)) {
				temp.add(cls.cast(a));
			}
		}
		return temp;
	}

	
}
