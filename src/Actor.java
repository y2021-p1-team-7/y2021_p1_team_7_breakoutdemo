import java.util.ArrayList;

import javafx.scene.image.ImageView;

public abstract class Actor extends ImageView{
	
	public Actor() {
		
	}
	
	public abstract void act(long now);
	
	public void move(double dx, double dy) {
		setX(getX() + dx);
		setY(getY() + dy);
	}
	
	public World getWorld() {
		return (World)getParent();
	}
	
	public double getWidth() {
		return getBoundsInLocal().getWidth();
	}
	
	public double getHeight() {
		return getBoundsInLocal().getHeight();
	}
	
	public <A extends Actor> java.util.List<A> getIntersectingObjects(java.lang.Class<A> cls){
		ArrayList<A> intersecting = new ArrayList<A>();
		for(A a : getWorld().getObjects(cls)) {
			a = cls.cast(a);
			if(a != this && a.intersects(getBoundsInLocal())) {
				intersecting.add(a);
			}
		}
		return intersecting;
	}
	
	public <A extends Actor> A getOneIntersectingObject(java.lang.Class<A> cls) {
		for(A a : getWorld().getObjects(cls)) {
			a = cls.cast(a);
			if(a != this && a.intersects(getBoundsInLocal())) {
				return a;
			}
		}
		return null;
	}

}
